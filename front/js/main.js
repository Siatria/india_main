let topInfo = document.querySelector('.top-casino__info')
let topBtn = document.querySelector('.top-casino__more-btn')

let newestInfo = document.querySelector('.newest-casinos__info')
let newestBtn = document.querySelector('.newest-casinos__more-btn')

let newSlotsInfo = document.querySelector('.newest-slots__info')
let newSlotsBtn = document.querySelector('.newest-slots__more-btn')

let promotionsInfo = document.querySelector('.promotions__info')
let promotionsBtn = document.querySelector('.promotions__more-btn')

let bestInfo = document.querySelector('.how__top__info')
let bestBtn = document.querySelector('.how__top-show-all')

topBtn.addEventListener('click', () => {
    topInfo.classList.toggle('toggle-list');
    topBtn.style.display = "none";
});

newestBtn.addEventListener('click', () => {
    newestInfo.classList.toggle('toggle-list');
    newestBtn.style.display = "none";
});

newSlotsBtn.addEventListener('click', () => {
    newSlotsInfo.classList.toggle('toggle-list');
    newSlotsBtn.style.display = "none";
});

promotionsBtn.addEventListener('click', () => {
    promotionsInfo.classList.toggle('toggle-list');
    promotionsBtn.style.display = "none";
});

bestBtn.addEventListener('click', () => {
    bestInfo.classList.toggle('toggle-list');
    bestBtn.style.display = "none";
});

// Open-close FAQ
let details = document.querySelectorAll("details");
for(let i = 0; i < details.length; i++) {
    details[i].addEventListener("toggle", accordion);
}
function accordion(event) {
    if (!event.target.open) return;
    let details = event.target.parentNode.children;
    for(let i = 0; i < details.length; i++) {
        if (details[i].tagName !== "DETAILS" ||
            !details[i].hasAttribute('open') ||
            event.target === details[i]) {
            continue;
        }
        details[i].removeAttribute("open");
    }
}

window.addEventListener('load', function () {
    document.addEventListener('click', function (e) {
        if (!!e.target.closest('#menu-toggle')) {
            e.preventDefault();
            e.stopPropagation();
            document.getElementById('header-mob').classList.toggle('open');
            document.querySelectorAll('.mob-menu__item.--dropdown.open')
                .forEach(el => {
                    el.classList.remove('open');
                });
            return;
        }

        if (!!e.target.closest('.mob-menu__item.--dropdown')) {
            e.preventDefault();
            e.stopPropagation();
            e.target.closest('.mob-menu__item.--dropdown').classList.add('open');
            return;
        }

        if (!!e.target.closest('.mob-menu__dropdown__top')) {
            e.preventDefault();
            e.stopPropagation();
            document.querySelector('.mob-menu__item.--dropdown.open').classList.remove('open');
        }
    });
}, {once: true})